#
# makefile for `ifdex'
#
VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

SOURCES = README COPYING NEWS ifdex ifdex.adoc ifdex.1 Makefile \
	control ifdex-logo.png smoketest.tst smoketest.chk

all: ifdex.1

.SUFFIXES: .html .adoc .1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -f *~ *.1 *.html

install: ifdex.1 uninstall
	install -m 0755 -d $(DESTDIR)/usr/bin
	install -m 0755 -d $(DESTDIR)/usr/share/man/man1
	install -m 0755 ifdex $(DESTDIR)/usr/bin/
	install -m 0644 ifdex.1 $(DESTDIR)/usr/share/man/man1/

uninstall:
	rm -f /usr/bin/ifdex /usr/share/man/man6/ifdex.1
	rm -f /usr/share/pixmaps/ifdex.png

pylint:
	@pylint --score=n ifdex

check:
	@./ifdex -c smoketest.tst | diff -u smoketest.chk -
	@echo "No diff output is good news."

rebuild:
	./ifdex -c smoketest.tst >smoketest.chk

version:
	@echo $(VERS)

ifdex-$(VERS).tar.xz: $(SOURCES)
	tar --transform='s:^:ifdex-$(VERS)/:' --show-transformed-names -cvzf ifdex-$(VERS).tar.gz $(SOURCES) ifdex.1

dist: ifdex-$(VERS).tar.xz

release: ifdex-$(VERS).tar.xz ifdex.html
	shipper version=$(VERS) | sh -e -x

refresh: ifdex.html
	shipper -N -w version=$(VERS) | sh -e -x
